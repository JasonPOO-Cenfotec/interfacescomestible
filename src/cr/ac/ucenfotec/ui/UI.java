package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.IEatable;
import cr.ac.ucenfotec.bl.Manzana;
import cr.ac.ucenfotec.bl.Perro;
import cr.ac.ucenfotec.bl.Piedra;

import java.util.ArrayList;

public class UI {

    public static void main(String[] args) {

        Perro perro1 = new Perro("Copito","Chiguagua");
        Piedra piedra1 = new Piedra("Rubí","Rojo");
        Manzana manzana1 = new Manzana("Gala",500);

        ArrayList<IEatable> listaCosas = new ArrayList<>();
        listaCosas.add(perro1);
        listaCosas.add(piedra1);
        listaCosas.add(manzana1);

        for (IEatable cosa:listaCosas) {
            System.out.println(cosa.isEatable());
        }
    }
}
