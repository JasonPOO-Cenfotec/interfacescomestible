package cr.ac.ucenfotec.bl;

public interface IEatable {
    // variables definidas aquí son constantes
    // métodos definidos aquí son public y abstract.

    public abstract String isEatable();
}
