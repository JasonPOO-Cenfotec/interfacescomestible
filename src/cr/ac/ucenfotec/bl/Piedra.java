package cr.ac.ucenfotec.bl;

import java.awt.*;

public class Piedra implements IEatable {

    private String nombre;
    private String color;

    public Piedra() {
    }

    public Piedra(String nombre, String color) {
        this.nombre = nombre;
        this.color = color;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String isEatable() {
        return "Yo soy una piedra de tipo " + nombre + ", y NOO soy comestible!";
    }

    public String toString() {
        return "Piedra{" +
                "nombre='" + nombre + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Piedra piedra = (Piedra) o;
        return nombre.equals(piedra.nombre) && color.equals(piedra.color);
    }

}
