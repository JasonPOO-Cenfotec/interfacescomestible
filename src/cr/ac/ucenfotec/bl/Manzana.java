package cr.ac.ucenfotec.bl;

import java.util.Objects;

public class Manzana implements IEatable {

    private String nombre;
    private double precio;

    public Manzana() {
    }

    public Manzana(String nombre, double precio) {
        this.nombre = nombre;
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String isEatable() {
        return " Yo soy una manzana de tipo " + nombre + ", y SÍ soy comestible!!";
    }

    public String toString() {
        return "Manzana{" +
                "nombre='" + nombre + '\'' +
                ", precio=" + precio +
                '}';
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Manzana manzana = (Manzana) o;
        return this.nombre.equals(manzana.nombre);
    }


}
